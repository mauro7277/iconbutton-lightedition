package org.m5s;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.event.dom.client.TouchEndHandler;

/**
 *
 * @author Vincenzo Abate
 *
 * @version $Id$
 */
public class IconButton extends Button {
    private String iconUrl;

    /**
     * Empty Constructor.
     */
    public IconButton() { }

    /**
     * Constructor.
     *
     * @param text Caption of the button.
     */
    public IconButton(String text) {
        setText(text);
    }

    /**
     * Constructor.
     *
     * @param text Caption of the button
     * @param e Handler of the ClickEvent
     */
    public IconButton(String text, TouchEndHandler e) {
        setText(text);
        addTouchEndHandler(e);
    }

    /**
     * Constructor.
     *
     * @param text Caption of the button
     * @param icon Icon to set up in the button
     * @param e Handler of the ClickEvent
     */
    public IconButton(String text, ImageResource icon, TouchEndHandler e) {
        setText(text);
        this.iconUrl = icon.getSafeUri().asString();
        addTouchEndHandler(e);
        String html = "<div vertical-align: middle><img src='" + this.iconUrl + "' style='margin-right: 16px'></img><label>" + getText()
                + "</label></div>";
        setHTML(html);
    }

    /**
     * Constructor.
     *
     * @param text Caption of the button
     * @param icon Icon to set up in the button
     */
    public IconButton(String text, ImageResource icon) {
        this.iconUrl = icon.getURL();
        String html = "<div vertical-align: middle><img src='" + this.iconUrl + "' style='margin-right: 16px'></img><label>" + getText()
                + "</label></div>";
        setHTML(html);
    }

    /**
     * Set image resource.
     *
     * @param imageResource Image resource relative to the icon you want to set.
     */
    public void setResource(ImageResource imageResource) {
        this.iconUrl = imageResource.getSafeUri().asString();       
        String html = "<div vertical-align: middle><img src='" + this.iconUrl + "' style='margin-right: 16px'></img><label>" + getText()
                + "</label></div>";
         setHTML(html);
    }

    /**
     * Get Icon URL.
     *
     * @return url of the icon to show
     */
    public String getIconURL() {
        return this.iconUrl;
    }
}
